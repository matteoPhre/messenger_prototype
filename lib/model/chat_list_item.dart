import 'package:flutter/material.dart';
import 'dart:convert';

class ChatListItem {
  final int id;
  final String avatarUrl;
  final String name;
  final String lastMessage;
  final DateTime timestamp;
  //final String lastSeen;

  ChatListItem.fromData(data)
      : this.id = int.tryParse(data["id"]),
        this.avatarUrl = data['avatarUrl'],
        this.name = data['name'],
        this.lastMessage = data['lastMessage'],
        this.timestamp = DateTime.now();
}
