import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:whatsapp_clone/model/chat_list_item.dart';
import './chat_list_tile.dart';
import './chat_room.dart';
import 'package:intl/intl.dart';
import '../home/home.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;

class ChatList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    ChatListState state = new ChatListState();
    state._connect();
    return state;
  }
}

class ChatListState extends State<ChatList> {
  String broker = "farmer.cloudmqtt.com";
  int port = 16107;
  String username = "ghivnnnd";
  String passwd = "LfE03MS1j8qS";
  String clientIdentifier = "android";

  mqtt.MqttClient client;
  mqtt.MqttConnectionState connectionState;
  List<ChatListItem> chatModelList = [];

  StreamSubscription subscription;

  _onTileTap(BuildContext context, ChatListItem item) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (BuildContext context) {
        return ChatRoom(chat: item);
      }),
    );
  }

  void _subscribeToTopic(String topic) {
    if (connectionState == mqtt.MqttConnectionState.connected) {
      print("[MQTT Client] Subscribing to ${topic.trim()}");
      client.subscribe(topic, mqtt.MqttQos.exactlyOnce);
    }
  }

  String getDaysDifferenceAndFormatData(DateTime data) {
    DateTime now = DateTime.now().toUtc();
    Duration difference = now.difference(data.toUtc());

    return difference.inDays > 1
        ? DateFormat("dd/MM/yyyy HH:mm").format(data)
        : DateFormat("HH:mm").format(data);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: chatModelList.length,
      itemBuilder: (BuildContext context, int index) {
        return ChatListTile(
          new CircleAvatar(
            foregroundColor: Theme.of(context).primaryColor,
            backgroundColor: Colors.grey,
            backgroundImage: new NetworkImage(chatModelList[index].avatarUrl),
          ),
          chatModelList[index].name,
          chatModelList[index].lastMessage,
          getDaysDifferenceAndFormatData(chatModelList[index].timestamp),
          () => _onTileTap(context, chatModelList[index]),
          avatarRadius: listLeadingAvatarRadius,
        );
      },
    );
  }

  void _connect() async {
    client = mqtt.MqttClient(broker, "");
    client.port = port;
    client.logging(on: true);
    client.keepAlivePeriod = 30;
    client.onDisconnected = _onDisconnected;

    final mqtt.MqttConnectMessage connMess = mqtt.MqttConnectMessage()
        .withClientIdentifier(clientIdentifier)
        .startClean() // Non persistent session for testing
        .keepAliveFor(30)
        .withWillQos(mqtt.MqttQos.atMostOnce);
    print('[MQTT client] MQTT client connecting....');
    client.connectionMessage = connMess;

    /// Connect the client, any errors here are communicated by raising of the appropriate exception. Note
    /// in some circumstances the broker will just disconnect us, see the spec about this, we however will
    /// never send malformed messages.

    try {
      await client.connect(username, passwd);
    } catch (e) {
      print(e);
      _disconnect();
    }

    /// Check if we are connected
    if (client.connectionState == mqtt.MqttConnectionState.connected) {
      print('[MQTT client] connected');
      setState(() {
        connectionState = client.connectionState;
      });
    } else {
      print('[MQTT client] ERROR: MQTT client connection failed - '
          'disconnecting, state is ${client.connectionState}');
      _disconnect();
    }

    /// The client has a change notifier object(see the Observable class) which we then listen to to get
    /// notifications of published updates to each subscribed topic.
    subscription = client.updates.listen(_onMessage);

    _subscribeToTopic("users/messages/+");
  }

  void _disconnect() {
    print('[MQTT client] _disconnect()');
    client.disconnect();
    _onDisconnected();
  }

  void _onDisconnected() {
    print('[MQTT client] _onDisconnected');
    setState(() {
      //topics.clear();
      connectionState = client.connectionState;
      client = null;
      subscription.cancel();
      subscription = null;
    });
    print('[MQTT client] MQTT client disconnected');
  }

  void _onMessage(List<mqtt.MqttReceivedMessage> event) {
    print(event.length);
    final mqtt.MqttPublishMessage recMess =
        event[0].payload as mqtt.MqttPublishMessage;
    final String message =
        mqtt.MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

    /// The above may seem a little convoluted for users only interested in the
    /// payload, some users however may be interested in the received publish message,
    /// lets not constrain ourselves yet until the package has been in the wild
    /// for a while.
    /// The payload is a byte buffer, this will be specific to the topic
    print('[MQTT client] MQTT message: topic is <${event[0].topic}>, '
        'payload is <-- ${message} -->');
    print(client.connectionState);
    print("[MQTT client] message with topic: ${event[0].topic}");
    print("[MQTT client] message with message: ${message}");
    setState(() {
      Map<String, dynamic> model = jsonDecode(message);
      ChatListItem chatmodel = ChatListItem.fromData(model);
      if (chatModelList.where((x) => x.id == chatmodel.id).toList().length >
          0) {
        chatModelList.removeWhere((x) => x.id == chatmodel.id);
      }
      chatModelList.add(chatmodel);

      // _received = message;
    });
  }
}
